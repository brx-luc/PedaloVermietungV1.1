![Use Cases Diagramm](images/usecase.jpg)


| Use Case No 1|  Reservieren |
| ----------- | ----------- |
| Pre-Condition  | - |
| Description of Use Case in detail (main success story)   | Der User sieht auf der Seite eine Liste mit freien Pedalos. Von diesen Pedalos kann er eines oder mehrere auswählen, um sie zu reservieren. Bei der Reservation wird auch der Zeitpunkt ausgewählt. Die Preise der Vermietung werden ebenfalls angezeigt. |
| Flow |<ol><li> Benutzer drückt in der Menubar auf den Punkt Reservieren </li><li> Benutzer wählt ein Datum und eine Zeit aus </li><li> Benutzer wählt Pedalo aus den verfügbaren Pedalos aus </li><li> Benutzer klickt auf den Button Reservieren </li><li> Benutzer gibt persönliche Daten ein </li><li> Benutzer drückt auf den Button Senden </li><li> Benutzer erhält eine Bestätigungsmail </li></ol>
| Post-Condition   | Datum und Zeit werden ausgewählt |
| Exceptions / alternate path   | Kann entweder per Telefon reservieren oder direkt vor Ort ein Pedalo mieten. |

| Use Case No 2|  Stornierung |
| ----------- | ----------- |
| Pre-Condition  | Eine Reservation wurde bestätigt.|
| Description of Use Case in detail (main success story)   | Durch irgendwelche Gründe muss die Reservation storniert werden. Die Stornierung muss mit Ja bestätigt werden. Dadurch wird die Reservation auch aus dem System gelöscht.|
| Flow | <ol><li> Benutzer drückt in der Menubar auf den Punkt Stornierung</li><li> Benutzer gibt die Reservationsnummer aus der Bestätigungsmail ein</li><li> Benutzer drückt auf den Button Stornieren</li><li> Benutzer bestätigt die Stornierung mit Ja</li><li> Benutzer gelangt auf die Bestätigungsseite</li><ol>
| Post-Condition   | - |
| Exceptions / alternate path   | Die Reservation wird nicht storniert. |

| Use Case No 3|  Admin-Seite |
| ----------- | ----------- |
| Pre-Condition  | Der Benutzer verfügt über ein Admin-Login |
| Description of Use Case in detail (main success story)   | Um eine Übersicht der Reservationen zu bekommen, muss sich der Admin über ein Login-Formular einloggen.|
| Flow | <ol><li> Admin klickt in der Fusszeile auf Login</li><li>Admin gibt den Benutzernamen und das Passwort ein</li><li> Admin drückt auf den Button Anmelden</li><li>Admin erhält eine Übersicht aller getätigten Reservationen</li></ol>
| Post-Condition   |  Reservationen werden bearbeitet |
| Exceptions / alternate path   | - |


| Use Case No 4|  Reservationsbearbeitung |
| ----------- | ----------- |
| Pre-Condition  | Die Reservationen sind abgeschlossen |
| Description of Use Case in detail (main success story)   | Bei der Reservationsübersicht gibt es einen Button, auf den der Admin klicken kann, um die Pedalos wieder freizugeben, sodass zukünftige Kunden die Pedalos wieder sehen und einen Button um Reservationen zu ändern.|
| Flow |Update:<ol><li>Admin klickt in der Zeile Bearbeiten auf Update</li><li>Admin ändert die Reservation entsprechend</li></ol>Delete:<ol><li>Admin klickt in der Zeile Bearbeiten auf Delete</li><li> Pedalo wird aus der Reservationsliste gelöscht und freigegeben</li></ol>
| Post-Condition   | Pedalos werden auf wieder zur Reservation freigegeben |
| Exceptions / alternate path   | - |


## Ablaufdiagram Reservation

![Ablaufdiagram](images/MicrosoftTeams-image__11_.png)


[Klassendiagram](klassendiagram.md)
