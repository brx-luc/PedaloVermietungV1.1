# Zustimmungsformular

Ich stimme zu, in der Studie von Ruthless Pedalo teilzunehmen.

Ich verstehe, dass die Teilnahme am Usability Test freiwilig ist und ich stimme ebenfalls zu, bei allfälligen Zweifeln oder unangenehmen Situationen während der Durchführung sofortige Meldung einzureichen.

Bitte unterzeichnen Sie an den entsprechenden stellen, um zu bestätigen, dass Sie die Teilnahmebedingungen gelesen und verstanden habne und dass allfällige Fragen über die Durchführung beantwortet wurden.

**Datum:** _______________________________

**Name:** ________________________________

**Unterschrif:**__________________________

**Vielen Dank für Ihre Teilnahme!**
