# Usability Testing
 
[Zustimmungsformular](zustimmungsformular.md)

## Szenario
Am 1. August ist schönes Wetter. Sie wollen deshalb mit ein paar Freunden auf dem Zürichsee ein Pedalo mieten.
Da Sie wissen, dass an diesem Tag viele die gleiche Idee haben werden, entscheiden Sie sich auf Nummer sicher zu gehen und ein Pedalo im Voraus zu reservieren.

## Feedback Person 1
| Thema |  Testergebnis | Verbesserungspotenzial |
| ----------- | ----------- | ----------- |
| Reservationsseite finden | OK | - |
| Pedalo auswählen und reservieren | OK | Checkboxen umbenennen |
| Persönliche Daten eingeben | OK | - |

## Feedback Person 2
| Thema |  Testergebnis | Verbesserungspotenzial |
| ----------- | ----------- | ----------- |
| Reservationsseite finden | OK | - |
| Pedalo auswählen | OK | einzelne Teile der Seite klar beschriften |
| Persönliche Daten eingeben | OK | - |

## Fazit
Unsere Testpersonen stiessen nur auf kleine Probleme auf unserer Webseite.
Die einzigen Probleme fanden sich auf der Reservationsseite. Die Darstellung war ein wenig verwirrend. So verstanden die Testpersonen nicht, für was die Checkboxen waren, da die Beschriftung nucht klar war. Ausserdem wurde uns empfohlen, die einzelnen Teile auf der Seite mit einem Titel oder einer kurzen Beschreibung zu ergänzen.
Dank dieser Feedbacks konnten wir unsere Wireframes nochmal anpassen und verbessern.

