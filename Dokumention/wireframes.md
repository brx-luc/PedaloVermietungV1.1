# Homepage 

![Homepage](images/Home.png)

# Reservationsseite

![Reservation1](images/Reservation1.png)

# Kontaktdaten des Benutzers

![Reservationsformular](images/Reservation2.png)

# Reservationsbestätigung

![Reservationsbestätigung](images/Reservation3.png)

# Stornierung

![Stornierung](images/Stornierung1.png)

# Bestätigung

![Stornierungbestätigung durch Benutzer](images/StornierungBestätigung.png)

# Stornierungsbestätigung

![Stornierungsbestätigung](images/Stornierung2.png)

# Admin-Login

![Admin-Login](images/Login.png)

# Adminseite

![Adminseite](images/Admin.png)

# AGB Seite

![AGB](images/AGB.png)

# Kontatk Seite

![Kontakt](images/Kontakt.png)

# Datenschutz

![Datenschutz](images/Datenschutz.png)

# Impressum

![Impressum](images/Impressumg.png)

# Rahmenbedingungen

![Rahmenbedingungen](images/Rahmenbedingung.png)

# Wetter

![Wetter](images/Wetter.png)

[UseCases](usecases.md)
