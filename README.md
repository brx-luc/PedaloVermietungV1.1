# Pedalovermietung

Unser Projekt soll die Reservation der Pedalos auf dem Zürisee erleichtern. Die Kunden sollen einfach ein Pedalo auswählen können, angeben, an welchem Datum und wie lange er das Pedalo mieten will. 
Der Administrator bekommt eine Übersicht über die reservierten Pedalos (und kann neue Pedalos im System hinzufügen). Sobald die Reservationszeit der Pedalos vorüber ist, kann der Administrator die Pedalos freigeben, sodass die Kunden wieder alle Pedalos sehen können. 


## Inhaltsverzeichnis

- [Wireframes](Dokumention/wireframes.md)
- [UseCases](Dokumention/usecases.md)
- [Domain Model und Klassendiagram](Dokumention/klassendiagram.md)
- [Usability Testing](Dokumention/UsabilityTesting.md)


## Zusammenfassung

Unsere Idee war das Design eines Frontend basierend auf dem Backend, welches wir im Modul 326 bei Herr Käser erstellt haben. Dabei sollten die Daten, welche gesendet werden in einem benutzerfreundlichen GUI dargestellt werden. Das Frontend sollte nach den besprochenen Design-Konzepten umgesetzt werden. Somit sollte das GUI möglichst ansprechend und einfach bedienbar sein. 
Die Idee kam uns, da es bis jetzt keine Webseite gibt, die die Reservation von Pedalos anbietet. Ausserdem passte diese Idee auch, weil sie vom Umfang her gross genug war und auch die nötige Komplexität vorhanden war. 

Wegen fehlenden fachlichen Kompetenzen realisierten wir nur das Design jedoch nicht die Implementation davon. Bei der Erstellung des Frontendes schritten wir nach konventionellen Abläufen voran. Zuerst notierten wir uns alle Usecases die es abzudecken gab, darauf erfolgte die Erstellung der low-fidelity Wireframes woraus wir die high-fidelity Wireframes entwickelten. Danach überlegten wir uns geeignete Usability-Tests und testeten die High-fidelity Wireframes auf Herz und Nieren mit verschiedenen motivierten Testern.  Mit den erhaltenen Ergebnissen verbesserten wir nun unsere Wireframes um die Benutzerfreundlichkeit und -erwartungen optimal zu decken.

Wir sind mit unserem Ergebnis zufrieden. Vor allem bezüglich des Aspektes, dass wir uns bereits im Vorhinein die Google-Services wie das Wetter und Google Maps überlegt haben und diese auch im Wireframe eingebunden haben. Ausserdem haben wir bereits an unsere Vernetzung in den Sozialen Medien gedacht und diese entsprechend in den Wireframes verlinkt. Wir glauben auch, dass unsere Struktur und unser Design Benutzerfreundlich und einfach bedienbar ist. Daher, dass sich Benutzer nicht anmelden müssen, um bei uns ein Pedalo zu reservieren, haben wir den Kundenkomfort optimal ausgeschöpft. Da es für unser Vorhaben nicht nötig ist, Kundenprofile zu erstellen, um diese für eMarketing oder ähnliches zu verwenden.
